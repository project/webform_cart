(function ($, Drupal) {
Drupal.AjaxCommands.prototype.DataLayerPush = function (ajax, response, status) {
  var pushAttr = response.data;
  pushAttr = pushAttr.replace(/ '/g, ' "')
      .replace(/{'/g, '{"')
      .replace(/'}/g, '"}')
      .replace(/', /g, '", ')
      .replace(/': /g, '": ')
      .replace(/n/g, "n")
      .replace(/"/g, '"')
      .replace(/&/g, "&")
      .replace(/r/g, "r")
      .replace(/t/g, "t")
      .replace(/b/g, "b")
      .replace(/f/g, "f");
  var pushAttrObj = JSON.parse(pushAttr);
  // For debugging purposes.
  window.dataLayer.push(pushAttrObj);
  // console.log(pushAttrObj);
}
})(jQuery, Drupal);