<?php

namespace Drupal\webform_cart\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Webform cart order type entities.
 */
interface WebformCartOrderTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
