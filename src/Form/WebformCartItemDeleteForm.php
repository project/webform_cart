<?php

namespace Drupal\webform_cart\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Webform cart item entity entities.
 *
 * @ingroup webform_cart
 */
class WebformCartItemDeleteForm extends ContentEntityDeleteForm {


}
